__author__ = 'Johnny Mao & Stanley Rabemananjara '

# ====================================
# ROCK PAPER SCISSORS CONTRACT CODE
# ====================================

rps_contract_code = '''
init:
    contract.storage[0] = msg.sender
    contract.storage[1] = 0 #number of players
code:

    # Indexes for ease of use
    # =======================
    NUM_P       = 1
    NUM_VERI    = 2
    PLAYER_ONE  = 4
    PLAYER_TWO  = 5
    P1_M_HASH   = 6
    P2_M_HASH   = 7
    P1_MOVE     = 9
    P2_MOVE     = 10

    # Available moves
    # =======================
    ROCK        = 0x72
    PAPER       = 0x70
    SCISSORS    = 0x73
    DISHONEST   = 0x58

    # Contract specific vars
    # =======================
    MAX_PLAYERS = 2
    PAYOUT      = 2*(10**23)

    # Join Request. Commit
    # =======================
    if (msg.datasize == 1):
        if (msg.value < 10**15):
            return(1)
        if (contract.storage[NUM_P] == MAX_PLAYERS):
            return(1)

        buyer = msg.sender
        contract.storage[NUM_P] = contract.storage[NUM_P] + 1
        contract.storage[3+contract.storage[NUM_P]] = buyer
        contract.storage[5+contract.storage[NUM_P]] = msg.data[0]
        return (0)

    # Reveal and payout
    # =======================
    elif (msg.datasize == 2):
        # Check if max players reached before verification
        if (contract.storage[NUM_P] < MAX_PLAYERS):
            return(2)
        
        # Verify player move
        player = msg.sender
        p_move = msg.data[1]
        veri_hash = sha3((msg.data[0] * 256) + p_move)
        legit_m = (p_move == ROCK or p_move == PAPER or p_move == SCISSORS)
        
        # Check if player one
        if (player == contract.storage[PLAYER_ONE]):
            contract.storage[NUM_VERI] = contract.storage[NUM_VERI] + 1
            if (contract.storage[P1_M_HASH] == veri_hash and legit_m):
                contract.storage[P1_MOVE] = p_move
            else:
                contract.storage[P1_MOVE] = DISHONEST
        
        # Check if player two
        elif (player == contract.storage[PLAYER_TWO]):
            contract.storage[NUM_VERI] = contract.storage[NUM_VERI] + 1
            if (contract.storage[P2_M_HASH] == veri_hash and legit_m):
                contract.storage[P2_MOVE] = p_move
            else:
                contract.storage[P2_MOVE] = DISHONEST

        # Who are you?
        else:
            return(42)

        # Check if all players went through verification
        if (contract.storage[NUM_VERI] < 2):
            return(1)

        p1_move = contract.storage[P1_MOVE]
        p2_move = contract.storage[P2_MOVE]

        # Verify player moves
        if (p1_move == DISHONEST and p2_move == DISHONEST):
            # Both are dishonest, send to contract owner
            send(1000, contract.storage[0], PAYOUT)
            return(0)
        elif (p1_move == DISHONEST and !(p2_move == DISHONEST)):
            # Send money to p2
            send(1000, contract.storage[PLAYER_TWO])
            return(PLAYER_TWO)
        elif (p2_move == DISHONEST and !(p1_move == DISHONEST)):
            # Send money to p1
            send(1000, contract.storage[PLAYER_ONE])
            return(PLAYER_ONE)

        # Players have tied
        if (p1_move == p2_move):
            # Contract owner gets the money
            send(1000, contract.storage[0], PAYOUT)
            return(0)
        # Player 2 wins
        elif (p2_move == PAPER and p1_move == ROCK):
            send(1000, contract.storage[PLAYER_TWO], PAYOUT)
            return(PLAYER_TWO)
        elif (p2_move == SCISSORS and p1_move == PAPER):
            send(1000, contract.storage[PLAYER_TWO], PAYOUT)
            return(PLAYER_TWO)
        elif (p2_move == ROCK and p1_move == SCISSORS):
            send(1000, contract.storage[PLAYER_TWO], PAYOUT)
            return(PLAYER_TWO)
        # Player 1 wins
        else:
            send(1000, contract.storage[PLAYER_ONE], PAYOUT)
            return(PLAYER_ONE)
'''

import sys
import serpent
import os
from pyethereum import transactions, blocks, processblock, utils

# ====================================
# CONSTANTS
# ====================================
MAX_TRIES      =  3
TOO_MANY_TRIES = "t"
ROCK           =  0
PAPER          =  1
SCISSORS       =  2
STR_LENGTH     =  31

# ====================================
# FUNCTION DEFINITIONS
# ====================================

# Retrieves user input.
# 0=ROCK, 1=PAPER, 2=SCISSORS
# User gets MAX_TRIES number of
# tries to enter valid input
def get_user_input(player):
    num_tries  = 0
    user_input = -1
    while (num_tries < MAX_TRIES):
        print ""
        print "{0}, please enter a move:".format(player)
        print "0. ROCK"
        print "1. PAPER"
        print "2. SCISSORS"
        try:
            user_input = int(raw_input())
            if (user_input < 0 or user_input > 2):
                num_tries += 1
            else:
                break
        except ValueError:
            num_tries += 1
    if (num_tries == MAX_TRIES):
        return TOO_MANY_TRIES
    else:
        return get_move_char(user_input)

# Retrieves the name of
# the corresponding character
# for the move based
# on its int value
def get_move_char(move):
    if (move == ROCK):
        return "r"
    elif (move == PAPER):
        return "p"
    else:
        return "s"

# Retrieves the name of the
# move based on the corresponding
# character
def get_move_name(move):
    if (move == "r"):
        return "ROCK"
    elif (move == "p"):
        return "PAPER"
    else:
        return "SCISSORS"

# ====================================
# MAIN PROGRAM ENTRY POINT STARTS HERE
# ====================================

# Compile code
rps = serpent.compile(rps_contract_code)

# Set up keys
first_key = utils.sha3('FIRST')
second_key = utils.sha3('SECOND')

# Create players
first_player = utils.privtoaddr(first_key)
second_player = utils.privtoaddr(second_key)

# Set up host
host_key = utils.sha3('HOST')
host = utils.privtoaddr(host_key)

# Initialize block
genesis = blocks.genesis({
        first_player: 10**30,
        second_player: 10**30,
        host: 10**20
})

# Initialize the contract
tx1 = transactions.contract(0,10**12,10000,0,rps).sign(host_key)
result, contract = processblock.apply_transaction(genesis, tx1)

# Player 1's Move
first_move = get_user_input("Player 1")

# Player dismissed if he/she tries too many times!
if (first_move == TOO_MANY_TRIES):
    print "Player 1 tried too many times!"
    sys.exit()

# Random string for player one
randstr1 = os.urandom(STR_LENGTH)

# Concatenate rnd str with move and hash
p1_hash = utils.sha3(randstr1+first_move)

# Player 1 Joins and commits
# Pass hash to contract
first_join = transactions.Transaction(
    0,
    10**12,
    10000,
    contract,
    10**23,
    serpent.encode_datalist([p1_hash])
).sign(first_key)
result, ans = processblock.apply_transaction(genesis,first_join)

# Player 2's Move
second_move = get_user_input("Player 2")

# Player dismissed if he/she tries too many times
if (second_move == TOO_MANY_TRIES):
    print "Player 2 tried too many times!"
    sys.exit()

# Random string for player two
randstr2 = os.urandom(STR_LENGTH)

# Concatenate rnd str with move and hash
p2_hash = utils.sha3(randstr2+second_move)

# Player 2 Joins
second_join = transactions.Transaction(
    0,
    10**12,
    10000,
    contract,
    10**23,
    serpent.encode_datalist([p2_hash])
).sign(second_key)
result, ans = processblock.apply_transaction(genesis,second_join)

# PLEASE NOTE:
# We are assuming that the players have a means of checking the contract for
# commitment from both players. However, in this implementation we already
# know that both players have commmited to the contract so we will continue with
# verification as if both players have already been alerted.

# Display current state information
print
print('Host Address: %s ' %str(hex(genesis.get_storage_data(contract, 0))))
print('=================================================================')
print('Player 1 Address: %s ' %str(hex(genesis.get_storage_data(contract, 4))))
print('Player 1 Balance: %s ' %str(genesis.get_balance(first_player)))
print('Player 1 Move   : %s ' %(get_move_name(first_move)))
print('=================================================================')
print('Player 2 Address: %s ' %str(hex(genesis.get_storage_data(contract, 5))))
print('Player 2 Balance: %s ' %str(genesis.get_balance(second_player)))
print('Player 2 Move   : %s ' %(get_move_name(second_move)))
print('=================================================================')

# Player 1 Verification
# Pass random string along with p1's move to contract
first_verify = transactions.Transaction(
    1,
    0,
    10000,
    contract,
    0,
    serpent.encode_datalist([randstr1, first_move])
).sign(first_key)
result, ans = processblock.apply_transaction(genesis,first_verify)

# Player 2 Verification
# Pass random string along with p2's move to contract
second_verify = transactions.Transaction(
    1,
    0,
    10000,
    contract,
    0,
    serpent.encode_datalist([randstr2, second_move])
).sign(second_key)
result, ans = processblock.apply_transaction(genesis,second_verify)

# Display winner and final balances
print('Winner address   : %s' %str(hex(genesis.get_storage_data(contract,ans))))
print('Player 1 Balance : %s' %(str(genesis.get_balance(first_player))))
print('Player 2 Balance : %s' %(str(genesis.get_balance(second_player))))

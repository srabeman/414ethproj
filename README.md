```python
authors = 'Johnny Mao & Stanley Rabemananjara'
```

# Rock Paper Scissors #
![RPS](http://cbskool2.files.wordpress.com/2011/08/rock-paper-scissors.jpg?w=420)

## **Mistakes from the past** ##

* Was not using a true random number generator
* Moves were randomized by the server (reduces incentive to play)

## **Improvements** ##

* Players can now put in their own move
* Usage of a true random string generator (urandom)
* Usage of a commitment scheme
* Cleaner code, much better documentation
* Enhanced report

## **Player moves** ###
* User can now play their own move
* They have the same move choices (R, P, S)
* They are limited to 3 tries to enter a valid move
```python
def get_user_input(player):
    num_tries  = 0
    user_input = -1
    while (num_tries < MAX_TRIES):
        print ""
        print "{0}, please enter a move:".format(player)
        print "0. ROCK"
        print "1. PAPER"
        print "2. SCISSORS"
        try:
            user_input = int(raw_input())
            if (user_input < 0 or user_input > 2):
                num_tries += 1
            else:
                break
        except ValueError:
            num_tries += 1
    if (num_tries == MAX_TRIES):
        return TOO_MANY_TRIES
    else:
        return get_move_char(user_input)
```

## **Commitment Scheme** ##
* Player picks a move (begins a commitment)
```python
first_move = get_user_input("Player 1")
```
* Random string gets generated for the player
```python
randstr1 = os.urandom(STR_LENGTH)
```

* Random string gets concatenated with player move and hashed (Hides move)
```python
p1_hash = utils.sha3(randstr1+first_move)
```
* Hash is sent and stored on the contract.
```python
first_join = transactions.Transaction(
    0,
    10**12,
    10000,
    contract,
    10**23,
    serpent.encode_datalist([p1_hash])
).sign(first_key)
result, ans = processblock.apply_transaction(genesis,first_join)
```
* Players get notified once contract has stored both hashes
* Player has to confirm their move by resubmitting rand string along with move
(Safe to display move in plain sight at this time)
```python
first_verify = transactions.Transaction(
    1,
    0,
    10000,
    contract,
    0,
    serpent.encode_datalist([randstr1, first_move])
).sign(first_key)
result, ans = processblock.apply_transaction(genesis,first_verify)
```
* Contract verifies the binding
```python
    elif (msg.datasize == 2):
        # Check if max players reached before verification
        if (contract.storage[NUM_P] < MAX_PLAYERS):
            return(2)
        
        # Verify player move
        player = msg.sender
        p_move = msg.data[1]
        veri_hash = sha3((msg.data[0] * 256) + p_move)
        legit_m = (p_move == ROCK or p_move == PAPER or p_move == SCISSORS)
        
        # Check if player one
        if (player == contract.storage[PLAYER_ONE]):
            contract.storage[NUM_VERI] = contract.storage[NUM_VERI] + 1
            if (contract.storage[P1_M_HASH] == veri_hash and legit_m):
                contract.storage[P1_MOVE] = p_move
            else:
                contract.storage[P1_MOVE] = DISHONEST
        
        # Check if player two
        elif (player == contract.storage[PLAYER_TWO]):
            contract.storage[NUM_VERI] = contract.storage[NUM_VERI] + 1
            if (contract.storage[P2_M_HASH] == veri_hash and legit_m):
                contract.storage[P2_MOVE] = p_move
            else:
                contract.storage[P2_MOVE] = DISHONEST

        # Who are you?
        else:
            return(42)
```

## **Difficulties encountered** ##
* utils.sha3() vs sepent sha3() behaved a bit differently due to padding
* Concatenating strings in serpent